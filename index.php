<?php
 include_once('header.html');
 $error = $_GET['error'];
 if(isset($error) || $error != NULL){
	echo "<script>
	iziToast.show({
		theme: 'dark',
		icon: 'icon-person',
		image: 'images/PMK_logo_material.png',
		message: 'Error en las credenciales!',
		position: 'topCenter', // bottomRight, bottomLeft, topRight, topLeft, topCenter, bottomCenter
		progressBarColor: 'rgb(0, 255, 184)'
	});
	</script>";
 }
?>
<div class="full-background login-page">
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-8 col-xs-12 col-md-offset-4 col-sm-offset-2 col-xs-offset-0">
					<form  action="php/login.php" method="post">
						<div class="card">
						<div class="head text-center"><img src="images/Claro-logo.png" alt="ClaroLogo" height="100px"></div>
						<div class="content">
							<div class="form-group">
								<label>Usuario</label>
								<input type="text" placeholder="Usuario" class="form-control" name="user" required>
							</div>
							<div class="form-group">
								<label>Contrase&ntilde;a</label>
								<input type="password" placeholder="Contrase&ntilde;a" class="form-control" name="password" required>
							</div>
						</div>
						<div class="text-center">
							<input type="submit" class="btn btn-fill btn-wd" value="Iniciar" id="btnLogin">
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
 include_once('footer.html');
?>
