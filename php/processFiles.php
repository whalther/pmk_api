<?php
    session_start();
    if(is_null($_SESSION['access_token']))
        header('Location: ../index.php');

    require_once('executeREST.php');
	$inputDocId = '9270629885a132a7cf3a707072905515';
	$taskId     = '9148559575a179bd85d6241000157367';
	$caseId     = '46509787255df8bc70f2f17099023224';
	$url        = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/input-document';
	$nFiles = count($_FILES['receipts']['name']);

	for ($i = 0; $i < $nFiles; $i++) {
	   $path = $_FILES['receipts']['tmp_name'][$i];
	   $type = $_FILES['receipts']['type'][$i];
	   $filename = $_FILES['receipts']['name'][$i];
	   //rename file from temp name to real name:
	   rename($path, sys_get_temp_dir() .'/'. $filename); //reverse \ for windows systems

	   $aVars = array(
	      'inp_doc_uid'     => $inputDocId,
	      'tas_uid'         => $taskId,
	      'app_doc_comment' => '',
	      'form'            => (phpversion() >= "5.5") ? new CurlFile($path, $type) : '@'.$path
	   );

	   $ch = curl_init();
	   curl_setopt($ch, CURLOPT_URL, $url);
	   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $oToken->access_token));
	   curl_setopt($ch, CURLOPT_POSTFIELDS, $aVars);
	   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	   $oResponse = json_decode(curl_exec($ch));
	   $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	   curl_close($ch);
	   unlink(sys_get_temp_dir() .'/'. $filename); //delete file
}
?>
