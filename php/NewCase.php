<?php
session_start();
if(is_null($_SESSION['access_token']))
    header('Location: ../index.php');
if(isset($_POST['tasUID'])){
	$tasUID = $_POST['tasUID'];
}

$pmServer = $_SESSION['url']; //set to address of the ProcessMaker server

function pmRestRequest($method, $endpoint, $aVars = null, $accessToken = null) {
   global $pmServer;

   if (empty($accessToken) and isset($_SESSION['access_token']))
      $accessToken = $_SESSION['access_token'];

   if (empty($accessToken)) { //if the access token has expired
      //To check if the PM login session has expired: !isset($_COOKIE['PHPSESSID'])
      header("Location: ../index.php"); //change to match your login method
      die();
   }

   //add beginning / to endpoint if it doesn't exist:
   if (!empty($endpoint) and $endpoint[0] != "/")
      $endpoint = "/" . $endpoint;

   $ch = curl_init($pmServer . $endpoint);
   curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $accessToken));
   curl_setopt($ch, CURLOPT_TIMEOUT, 30);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   $method = strtoupper($method);

   switch ($method) {
      case "GET":
         break;
      case "DELETE":
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
         break;
      case "PUT":
         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
      case "POST":
         curl_setopt($ch, CURLOPT_POST, 1);
         curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($aVars));
         break;
      default:
         throw new Exception("Error: Invalid HTTP method '$method' $endpoint");
         return null;
   }

   $oRet = new StdClass;
   $oRet->response = json_decode(curl_exec($ch));
   $oRet->status   = curl_getinfo($ch, CURLINFO_HTTP_CODE);
   curl_close($ch);

   if ($oRet->status == 401) { //if session has expired or bad login:
      header("Location: ../login.php"); //change to match your login method
      die();
   }
   elseif ($oRet->status != 200 and $oRet->status != 201) { //if error
      if ($oRet->response and isset($oRet->response->error)) {
         print "Error in $pmServer:\nCode: {$oRet->response->error->code}\n" .
               "Message: {$oRet->response->error->message}\n";
      }
      else {
         print "Error: HTTP status code: $oRet->status\n";
      }
   }

   return $oRet;
}

$aVars = array(
   'pro_uid'   => '8755798335a0f0d91e7aa98093953088',
   'tas_uid'   => '7974853515a0f0e5b89ea97008445689'
);
$oRet = pmRestRequest('POST', '/api/1.0/pruebas/cases', $aVars);

if ($oRet->status == 200) {
   print "New Case {$oRet->response->app_number} created.\n";
}

header("Location: list.php");
?>
