<?php
    session_start();
    if(is_null($_SESSION['access_token'])){
	    header('Location: ../index.php');
    }
    require_once('executeREST.php');
    $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases';
    $case_list = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );

    if(isset($_GET)){
    	$proj = $_GET['proj'];
    	$task = $_GET['task'];
    	$app = $_GET['app'];
    }
?>
<!DOCTYPE html>
<html lang="es-sv">
	<head>
		<link rel="icon" href="../images/Claro.svg.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="author" content="mario.carranza@onelinkbpo.com">
		<meta name="owner" content="Mario Carranza">
		<meta name="keywords" content="">
		<meta name="robots" content="index, follow">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="../css/paper.css">
		<link rel="stylesheet" href="../css/sweetalert2.min.css">
		<link rel="stylesheet" href="../css/style.css">
	</head>
	<body>
		<main>
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img src="../images/Claro-logo.png" alt="claro logo" height="60px"></a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="list.php"><i class="material-icons right">view_list</i>Lista de casos</a></li>
						<li><a href="searchPage.php"><i class="material-icons right">search</i>Busqueda de casos</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</div>
			</nav>
			<div class="header">
				<div class="row">
				</div>
	          </div><br>
			<div id="contenedor">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-sm-offset-2">
							<h3 class="title center">Menu</h3>
							<div class="list-group table-of-contents">
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=7972412105a1302da9ee7e1022134495&doc=9270629885a132a7cf3a707072905515&inputVariable=FileAdjuntosAPD" ?>">Activacion de paquete de datos</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=3455505375a1c7c4e82ced3085675580&doc=8549194335a1c7ed76f48f9038862085&inputVariable=FileAdjuntosBloqSmsContenido" ?>">Bloqueo SMS contenido y suscripcion</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=8074340825a1c4eddb06af5076774708&doc=9716491695a1c51e279baf2020789471&inputVariable=FileAdjuntosBloSerAdicionales" ?>">Bloqueo de servicios</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=3595886235a1c84f416a4c2036439612&doc=7820739745a1c87f5c3ab99009207239&inputVariable=FileAdjuntosCambDemograficos" ?>">Cambios demograficos</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=2125549315a1341e74c9e18016975670&doc=9202270505a134f1f201ab4006094812&inputVariable=FileAdjuntosCC" ?>">Cambio de ciclo</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=9495849605a1c2d263855a1002611614&doc=5668945315a1c2f6898efc6094578491&inputVariable=FileAdjuntosCambElegidos" ?>">Cambio de elegidos</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=7854003905a1c72c577a100011421724&doc=2251586915a1c7437557308038311871&inputVariable=FileAdjuntosCamMin" ?>">Cambio de Min</a>
								<!-- <a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=5819396975a1c1c316ce941064380546&doc=1173095705a1c1f93259f48039140171&inputVariable=FileAdjuntosCPInmediato" ?>">Proteccion Movil</a> -->
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=7609252925a14ce6b15cf25055333641&doc=4659729005a14d537610121030709545&inputVariable=FileAdjuntosCerGar" ?>">Certificacion garantia</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=7146606445a14d5b41225a2053501179&doc=9178466795a14dcd1d14f21017383616&inputVariable=FileAdjuntosCruceMin" ?>">Cruce Min</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=3139648435a14e6458198e0051510748&doc=6413879395a14ec4c358c24027145335&inputVariable=FileAdjuntosSoliCuponPago" ?>">Cupon pago</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=9827575685a1c8cf95693e6005347039&doc=3587646485a1c8e804c0bc2037108326&inputVariable=FileAdjuntosAcPaqDatos" ?>">Desactivacion paquete datos</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=5819396975a1c1c316ce941064380546&doc=1173095705a1c1f93259f48039140171&inputVariable=FileAdjuntosDesProMovil" ?>">Desactivacion Proteccion Movil</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=9541320805a15eebc5e11e2018173039&doc=1285400395a16e4e460af15051960746&inputVariable=FileAdjuntosElimCuo" ?>">Eliminacion cuotas por pago</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=4872629805a370cae7f5d92076295605&doc=7442473705a391bcb4e8071042942018&inputVariable=FileAdjuntosOtros" ?>">Otros</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=7840270995a1c3b159be245003351850&doc=6893310545a1c41b2b52fe0089230685&inputVariable=FileAdjuntosDesSerAdicionales" ?>">Desbloqueo de Servicios Adicionales</a>
								<a class="list-group-item" href="<?php echo "dynaform.php?proj=".$proj."&task=".$task."&app=".$app."&dyn=2590942385a1726969abcf3057990914&doc=6867065425a1b6af31462c5023445036&inputVariable=FileDesDupli" ?>">IMEI Duplicado</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="../js/sweetalert2.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<!-- <script src="../js/init.js"></script> -->
	<script>
	$(document).ready(function(){
		// $("#checkAPD input").val("1");
	});
	</script>
	</body>
</html>
