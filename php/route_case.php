<?php
	session_start();
	if(is_null($_SESSION['access_token'])){
	    header('Location: ../index.php');
	}

	require_once('executeREST.php');
	$url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/variable';
	executeREST( $url, 'PUT', $_POST, $_SESSION['access_token'] );
	

	$inputVariable = $_GET['inputVariable'];

	$inputDocId = $_GET['doc'];
	$taskId     = $_GET['task'];
	$url = $_SESSION['url'].'api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/input-document';
	$nFiles = count($_FILES["".$inputVariable.""]['name']);
    	for ($i = 0; $i < $nFiles; $i++) {
		$path = $_FILES["".$inputVariable.""]['tmp_name'][$i];
		$type = $_FILES["".$inputVariable.""]['type'][$i];
		$filename = $_FILES["".$inputVariable.""]['name'][$i];

		//move_uploaded_file($path, "/opt/processmaker/workflow/public_html/ProcessMakerApi/files/".$filename);
		move_uploaded_file($path, "/site/wwwroot/files/".$filename);
		

		//rename file from temp name to real name:
		//rename($path, "/tmp/". $filename); //reverse \ for windows systems
		$path = "/site/wwwroot/files/".$filename;
		echo "<br>path:<br>";
		print_r($path);
		$aVars = array(
   			'inp_doc_uid'     => $inputDocId,
   			'tas_uid'         => $taskId,
   			'app_doc_comment' => 'Documento Subido desde la Api',
   			'form'            => (phpversion() >= "5.5") ? new CurlFile($path, $type) : '@'.$path
		);		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer ' . $_SESSION['access_token']));
		curl_setopt($ch, CURLOPT_POSTFIELDS, $aVars);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$oResponse = json_decode(curl_exec($ch));
		$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		if (curl_errno($ch)) {
			print curl_error($ch);
		}
		curl_close($ch);
		unlink("/site/wwwroot/files/".$filename); //delete file
	}
	if($httpStatus == 200){
		echo "Subido";
	}
	echo "<br>httpdStatus:<br>";
	print_r($httpStatus);
	echo "<br>OResponse:<br>";
	print_r($oResponse);


	$url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/route-case';
	executeREST( $url, 'PUT', $_POST, $_SESSION['access_token'] );

	$getCVS = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/execute-trigger/6839027285a654724303bf4075587336';
	executeREST( $getCVS, 'PUT', $_POST, $_SESSION['access_token'] );
	
	$url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/execute-trigger/1439667415a90e816bd01d3071390123';
	executeREST( $url, 'PUT', $_POST, $_SESSION['access_token'] );

	header("Location: list.php");
?>
