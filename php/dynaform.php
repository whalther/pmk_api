<?php
    session_start();
    if(is_null($_SESSION['access_token'])){
	    header('Location: ../index.php');
    }
    require_once('executeREST.php');
    $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/project/'.$_GET['proj'].'/activity/'.$_GET['task'].'/steps';
    $steps = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );

    $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/project/'.$_GET['proj'].'/dynaform/'.$_GET['dyn'].'';
    $dynaform = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );

    $url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/execute-trigger/1911621095a157fae5bcc09047048477';
    executeREST( $url, 'PUT', $_POST, $_SESSION['access_token'] );

    //$url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/project/'.$_GET['proj'].'/dynaform/'.$_GET['dyn'].'/grid/gridRelacionarElegidosCambElegidos/field-definitions';
    //$oRet = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );


	$url = $_SESSION['url'].'/api/1.0/'.$_SESSION['ws'].'/cases/'.$_GET['app'].'/variables';
	$variables = executeREST( $url, 'GET', array(), $_SESSION['access_token'] );

	$inputVariable = $_GET["inputVariable"];

	if ($_GET['dyn'] == "7972412105a1302da9ee7e1022134495") {
		$validate = "validateAPD";
	}elseif ($_GET['dyn'] == "3455505375a1c7c4e82ced3085675580") {
		$validate = "validateBloqMMS";
	}elseif ($_GET['dyn'] == "8074340825a1c4eddb06af5076774708") {
		$validate = "validateBloSerAdicionales";
	}elseif ($_GET['dyn'] == "3595886235a1c84f416a4c2036439612") {
		$validate = "validateCambDemograficos";
	}elseif ($_GET['dyn'] == "2125549315a1341e74c9e18016975670") {
		$validate = "validateCamCiclo";
	}elseif ($_GET['dyn'] == "9495849605a1c2d263855a1002611614") {
		$validate = "validateCambElegidos";
	}elseif ($_GET['dyn'] == "8308105465a13506d1f9db2023058922") {
		$validate = "validateCT";
	}elseif ($_GET['dyn'] == "7854003905a1c72c577a100011421724") {
		$validate = "validateCamMin";
	}elseif ($_GET['dyn'] == "1480102535a1c6764d82fc2017754853") {
		$validate = "validateCambPlanFecha";
	}elseif ($_GET['dyn'] == "5476379395a1c1485caa944041629623") {
		$validate = "validateCPInmediato";
	}elseif ($_GET['dyn'] == "7609252925a14ce6b15cf25055333641") {
		$validate = "validateCerGar";
	}elseif ($_GET['dyn'] == "7146606445a14d5b41225a2053501179") {
		$validate = "validateCruceMin";
	}elseif ($_GET['dyn'] == "3139648435a14e6458198e0051510748") {
		$validate = "validateSoliCuponPago";
	}elseif ($_GET['dyn'] == "9827575685a1c8cf95693e6005347039") {
		$validate = "validateAcPaqDatos";
	}elseif ($_GET['dyn'] == "5819396975a1c1c316ce941064380546") {
		$validate = "validateDesProMovil";
	}elseif ($_GET['dyn'] == "9541320805a15eebc5e11e2018173039") {
		$validate = "validateElimCuo";
	}elseif ($_GET['dyn'] == "4872629805a370cae7f5d92076295605") {
		$validate = "validateOtros";
	}elseif ($_GET['dyn'] == "7840270995a1c3b159be245003351850") {
		$validate = "validateDesSerAdicionales";
	}elseif ($_GET['dyn'] == "2590942385a1726969abcf3057990914") {
		$validate = "validateDesDupli";
	}
?>
<!DOCTYPE html>
<html lang="es-sv">
	<head>
		<link rel="icon" href="../images/Claro.svg.png">
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="author" content="mario.carranza@onelinkbpo.com">
		<meta name="owner" content="Mario Carranza">
		<meta name="keywords" content="">
		<meta name="robots" content="index, follow">
		<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
		<link rel="stylesheet" href="../css/paper.css">
		<link rel="stylesheet" href="../css/sweetalert2.min.css">
		<link rel="stylesheet" href="../css/style.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
	</head>
	<body>
		<main>
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><img src="../images/Claro-logo.png" alt="claro logo" height="60px"></a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<li><a href="list.php"><i class="material-icons right">view_list</i>Lista de casos</a></li>
						<li><a href="searchPage.php"><i class="material-icons right">search</i>Busqueda de casos</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						<li><a href="logout.php">Logout</a></li>
					</ul>
				</div>
			</nav>
			<div class="header">
				<div class="row">
				</div>
	          </div><br>
			<div id="contenedor">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<h3 class="title center"><?php echo $dynaform['dyn_title'] ?></h3>
							<div class="panel panel-default">
								<div class="panel-body">
									<form action="route_case.php?app=<?php echo "".$_GET['app']."&task=".$_GET['task']."&doc=".$_GET['doc']."&inputVariable=".$_GET['inputVariable'].""; ?>" enctype="multipart/form-data" method="post" id='case1' name="case1">
										<table align="center" class="table" style="background-color:#fff; width: 100%;" border="0">
											<?php $rows = json_decode($dynaform['dyn_content'], true);
												foreach($rows['items'][0]['items'] as $field) {
													echo "<tr>";
													for($i = 0 ; $i<=count($field)-1 ; $i++){
														if(isset($field[$i]['mode'])){
															$mode = $field[$i]['mode'] == 'view'? " disabled" : "";
														}
														if(isset($field[$i]['var_name'])){
															$value = array_key_exists($field[$i]['var_name'], $variables)? $variables[$field[$i]['var_name']] : "";
														}
														switch(@$field[$i]['type']){
															case "title":
																echo "<th style='background-color:#1192d4; color:#FFF; font-size: 17px; box-shadow: 0px 2px 5px rgba(0,0,0,0.16), 0px 2px 10px rgba(0,0,0,0.12);' colspan='".$field[$i]['colSpan']."' >".$field[$i]['label']."</th>";
															break;
															case "subtitle":
																echo "<th align='center' colspan='".$field[$i]['colSpan']."' >".$field[$i]['label']."</th>";
															break;
															case "datetime":
															if($field[$i]['id']=="fechaPagoSoliCuponPago"){
																$dis = "";
																$type = "date";
															}else {
																$dis = "disabled='disabled'";
																$type = "datetime";
															}
																echo "<td colspan='".$field[$i]['colSpan']."'>
																<fieldset ".$dis." id='fieldset'>
																	<div class='form-group'>
																	<div class='input-group date' id='".$field[$i]['id']."'>
																	<label>".$field[$i]['label']."</label>
																	<input type='".$type."' class='form-control' value='".$value."' name='".$field[$i]['var_name']."' ".$dis."/>
																	</div>".$mode."
																	</div>
																</fieldset>
																</td>";
															break;
															case "text":
																echo "<td colspan='".$field[$i]['colSpan']."'><div class='form-group'><label>".$field[$i]['label']."</label>
																<input class='form-control'".$mode." value='".$value."' name='".$field[$i]['var_name']."' id='".$field[$i]['id']."' required></div>
																</td>";
															break;
															case 'hidden':
															echo "
																<input type='hidden' value='".$value."' name='".$field[$i]['var_name']."' id='".$field[$i]['id']."'>";
																break;
															case "checkbox":
																echo "<td colspan='".$field[$i]['colSpan']."' id='".$field[$i]['id']."'><div class='checkbox'><label>
																<input type='checkbox' value=''  ".$mode." name='".$field[$i]['var_name']."' id='".$field[$i]['id']."'/>".$field[$i]['label']."</label></div></td>";
															break;
															case "checkgroup":
																echo "<td colspan='".$field[$i]['colSpan']."' id='".$field[$i]['id']."'>";
																foreach($field[$i]['options'] as $option){
																	echo "<div class='checkbox'><label>
																	<input type='checkbox' value='".$option['value']."'  ".$mode." name='".$field[$i]['var_name']."' id='".$field[$i]['id']."'/>".$option['label']."</label></div>&nbsp;<br>";
																}
																echo "</td>";
															break;
															case "label":
																echo "<th align='center' colspan='".$field[$i]['colSpan']."' >".$field[$i]['label']."</th>";
															break;
															case "dropdown":
																echo "<td colspan='".$field[$i]['colSpan']."' id='".$field[$i]['id']."'><label>".$field[$i]['label']."</label><select name='".$field[$i]['var_name']."' class='form-control'".$mode." id='".$field[$i]['id']."' required>";
																echo "<option value='' selected disabled>Seleccionar</option>";
																foreach($field[$i]['options'] as $option){
																	$selected = $value == $option['value']? "selected" : "";
																	echo "<option value='".$option['value']."' ".$selected.">".$option['label']."</option>";
																}
																echo    "</select></td>";
															break;
															case "textarea":
																echo "<td colspan='".$field[$i]['colSpan']."'><label>".$field[$i]['label']."</label>
																<textarea class='form-control' rows='5' id='".$field[$i]['id']."' ".$mode." value='".$value."' name='".$field[$i]['var_name']."'></textarea>";
																echo    "</td>";
															break;
															// case "grid":
															// 	echo "<td colspan='".$field[$i]['colSpan']."'><label>".$field[$i]['label']."</label>
															// 	<table id='".$field[$i]['id']."' ".$mode." value='".$value."' name='".$field[$i]['var_name']."'>";
															// 	foreach($oRet as $oField) {
															// 		print "<th style='background-color: #000; color: #fff;'>".$oField['id']."</th>";
															// 	}
															// 	echo "
															// 	</table>";
															// 	echo    "</td>";
															// 		print $oField['id'];
															// break;
															case "multipleFile":
																echo "<td colspan='".$field[$i]['colSpan']."'><label>".$field[$i]['label']."</label>
																<input type='file' multiple='multiple' accept='.pdf,.jpg,.png,.docx,.doc,.xlsx' class='btn btn-primary ' value='".$value."' name='".$field[$i]['var_name']."[]'>";
																echo "Datos permitidos: pdf, jpg, png, docx, doc, xlsx";
																echo    "</td>";
															break;
															case "submit":
																echo "<td align='center' colspan='".$field[$i]['colSpan']."'><input class='btn btn-primary' type='submit' value='".$field[$i]['label']."'></td>";
															break;
															case "button":
																echo "<td align='center' colspan='".$field[$i]['colSpan']."'><input class='btn btn-warning' type='button' value='".$field[$i]['label']."' id='".$field[$i]['id']."'></td>";
															break;
														}
													}
													echo "</tr>";
												}
											?>
											<tr>
												<td align='center' colspan='12'><input class='btn btn-primary' type='submit' value='Enviar'>
												<!-- <div align='left'><a href="menu.php?" class="btn btn-info">Men&uacute;</a></div><br> -->
												<div align='left'><a href="list.php" class="btn btn-danger">Cancelar</a></div></td>
											</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</main>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script src="../js/sweetalert2.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js'></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jqBootstrapValidation/1.3.7/jqBootstrapValidation.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
	<script src="../js/validation.js"></script>
	<?php echo "<script>".$validate."();</script>" ?>
	</body>
</html>
