<?php
  session_start();
  if(empty($_SESSION)){
	  header('Location: ../index.php');
  }
if(isset($_POST['SubmitButton'])){
	$input = $_POST['inputText'];
	$date_from = $_POST['date_from'];
	$date_to = $_POST['date_to'];
}
  @$ch = curl_init("https://bpmtest.onelinkbpo.com/api/1.0/pruebas/cases/advanced-search/paged?start=1&search=$input&date_from=$date_from&date_to=$date_to");
  curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $_SESSION['access_token']));
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $oPage = json_decode(curl_exec($ch));
  $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);
?>
<!DOCTYPE html>
<html lang="es-sv">
<head>
	<link rel="icon" href="../images/Claro.svg.png">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="author" content="mario.carranza@onelinkbpo.com">
	<meta name="owner" content="Mario Carranza">
	<meta name="keywords" content="">
	<meta name="robots" content="index, follow">
	<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="../css/paper.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="../css/sweetalert2.min.css">
	<link rel="stylesheet" href="../css/myDataTables.css">
	<link rel="stylesheet" href="../css/style.css">
	<link rel="stylesheet" href="../css/preloader.css">
</head>
<body>
	<main>
		<!-- Page Loader -->
		<div class="page-loader-wrapper">
			<div class="loader">
				<div class="preloader">
					<div class="spinner-layer pl-red">
						<div class="circle-clipper left">
							<div class="circle"></div>
						</div>
						<div class="circle-clipper right">
							<div class="circle"></div>
						</div>
					</div>
				</div>
				<p>Cargando.</p>
			</div>
		</div>
		<!-- #END# Page Loader -->
		<nav class="navbar navbar-default">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img src="../images/Claro-logo.png" alt="claro logo" height="60px"></a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li><a href="list.php"><i class="material-icons right">view_list</i>Lista de casos</a></li>
					<li class="active"><a href="searchPage.php"><i class="material-icons right">search</i>Busqueda de casos</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="logout.php">Logout</a></li>
				</ul>
			</div>
		</nav>
		<div class="header">
			<div class="row">
			</div>
          </div><br>
		<div id="contenedor">
			<div class="fixed-action-btn" data-toggle="tooltip" data-placement="left" title="Filtrar" data-original-title="Tooltip on left" id="agregarCaso">
				<button id="" type="button" name="" class="btn btn-floating btn-large" style="background-color: #009688;" data-toggle="modal" data-target="#myModal"><i class="material-icons">filter_list</i></button>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="title center">Busqueda de casos</h3>
						<div class="panel panel-default">
							<div class="panel-body">
								<table id="tablaCasos" class="table table-striped table-hover table-bordered display order-column compact" cellspacing="0" width="100%">
									<thead>
											<th>Numero Caso</th>
											<th>Usuario actual</th>
											<th>Ultima modificaci&oacute;n</th>
											<th>Vencimiento</th>
											<th>Estado</th>
									</thead>
									<tbody>
									<?php
										if ($statusCode == 200) {
											//Cambiar estado del lenguaje ingles a espanol
											foreach ($oPage->data as $oCase) {
												if($oCase->app_status_label=="Completed"){
													$oCase->app_status_label = "Completado";
												}elseif ($oCase->app_status_label=="Canceled") {
													$oCase->app_status_label = "Cancelado";
												}elseif ($oCase->app_status_label=="To do") {
													$oCase->app_status_label = "Por hacer";
												}elseif ($oCase->app_status_label=="Draft") {
													$oCase->app_status_label = "Borrador";
												}
												//Formato para la fecha de expiracion y ultima actualizacion del caso
												$dueDate=date_create($oCase->del_task_due_date);
												$updateDate=date_create($oCase->app_update_date);
												print "<tr>";
												 print "
												 <td>$oCase->app_number</td>
												 <td>$oCase->app_current_user</td>
												 <td> ".date_format($updateDate, 'Y-m-d H:m:s')." </td>
												 <td> ".date_format($dueDate, 'Y-m-d H:m:s')." </td>
												 <td>$oCase->app_status_label</td>
												 ";
												 print "</tr>";
											}
										}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Ultima modificaci&oacute;n</h4>
							</div>
							<div class="modal-body">
								<form class="" action="searchPage.php" method="post" id="">
									<div class="form-group">
										<label>Caso</label>
										<input type="text" name="inputText" class="form-control"/>
									</div>
									<div class="form-group">
										<label>Fecha inicio</label>
										<input type="date" name="date_from" class="form-control"/>
									</div>
									<div class="form-group">
										<label>Fecha fin</label>
										<input type="date" name="date_to" class="form-control"/>
									</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary" name="SubmitButton">Filtrar</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	   </main>
	<script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
     <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.material.min.js"></script>
	<!-- <script src="../js/sweetalert2.min.js"></script> -->
	<script src="https://unpkg.com/sweetalert2@7.0.7/dist/sweetalert2.all.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/init.js"></script>
   </body>
</html>
